# Prizeship

Neste projeto foi desenvolvido um jogo virtual denominado Prizeship, seguindo os requisitos apresentados pela disciplina de Análise de Projeto de Sistemas. 

Prizeship é um jogo onde cada usuário palpita a posição final de uma  nave pilotada pela comunidade de jogadores. 
A nave movimenta-se em um ângulo, ascendente ou descendente, baseada em uma votação que fecha todos os dias em um certo horário.

Cada jogador pode realizar as seguintes ações desde que possuam créditos para isso:
* votar para a nave subir ou descer;
* antecipar a data do final do jogo;
* adquirir créditos ou enviá-los para outros jogadores;

Ganham os jogadores que acertarem a posição da nave ao final do jogo, que ocorre em uma determinada data que pode ser alterada de acordo com as ações dos jogadores.
