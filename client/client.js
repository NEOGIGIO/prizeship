var interval;

class Tela {
  constructor(button) {
    this._Botao = button;
  }

  atualizaSubir(msg) {
    if (msg != null)
      // console.log("subir-atualiza")
      $("#poll-up").text(msg.poll.subir);
  }

  atualizaDescer(msg) {
    if (msg != null)
      // console.log("descer-atualiza")
      $("#poll-down").text(msg.poll.descer);
  }

  atualizaAltitude(msg) {
    if (msg) $("#altitude").text("Altitude: " + msg.nave.altitude);
  }

  atualizaInterval(reference, duration) {
    if (reference != null && duration != null) {
      if (interval) {
        clearInterval(interval);
      }
      var start = new Date(reference);
      interval = setInterval(function() {
        $("#cd-timer").text(
          Math.round((start - new Date() + duration) / 1000, 0) + " Segundos"
        );
      }, 1000);
    }
  }

  atualizaDeadlineTimer(deadline) {
    if (deadline != null) {
      var deadline = new Date(deadline);
      $("#deadline-timer").text(formatDate(deadline));
    }
  }

  alteraSkin(nave) {
    if (nave != null) this.customizeShip(nave);
  }

  customizeShip(nave) {
    if (nave != null) $("#img-nave").attr("src", "./nave" + nave + ".png");
  }

  mostraInfo() {
    console.log("mostrando info");
  }

  mostraChart() {
    console.log("mostrando chart");
  }
}

class Botao {
  constructor() {}

  setTela(tela) {
    if (tela != null) this.tela = tela;
  }

  subir(socket) {
    if (socket != null)
      // console.log("subir-botao")
      return () => {
        socket.emit("subir");
      };
  }

  descer(socket) {
    if (socket != null)
      // console.log("descer-botao")
      return () => {
        socket.emit("descer");
      };
  }

  reduzir(socket) {
    if (socket != null)
      // console.log("reduzir-botao")
      return () => {
        socket.emit("reduzir");
      };
  }

  abrirChart() {
    return () => {
      this.tela.mostraChart();
    };
  }

  abrirDropdown() {
    return () => {
      console.log("dropdown aberto");
      this.tela.mostraInfo();
    };
  }

  comprarCreditos(creditos) {
    if (creditos != null)
      alert($("#quantidade-comprar").val() + " creditos comprados");
  }

  doarCreditos(value) {
    if (value != null)
      alert(
        $("#quantidade-doar").val() +
          " creditos doados para " +
          $("#pessoa-doar").val()
      );
  }

  changeUsername() {
    alert("Nome de usuário mudado");
  }

  changePassword() {
    alert("Senha mudada");
  }

  changeNave(nave) {
    if (nave != null)
      return () => {
        this.tela.alteraSkin(nave);
      };
  }
}

$(document).ready(function() {
  const io = require("socket.io-client");
  // or with import syntax

  const socket = io("http://localhost:3000");

  const button = new Botao();
  const screen = new Tela(button);
  button.setTela(screen);

  socket.on("updateall", msg => {
    console.log("values", msg);
    screen.atualizaDeadlineTimer(msg.deadline_timer.values.deadline);
    screen.atualizaInterval(
      msg.cd_timer.values.reference,
      msg.cd_timer.values.duration
    );
    screen.atualizaSubir(msg);
    screen.atualizaDescer(msg);
    screen.atualizaAltitude(msg);
  });

  $("#subir-btn").click(button.subir(socket));
  $("#descer-btn").click(button.descer(socket));
  $("#reduce").click(button.reduzir(socket));
  $("#botao-dropdown").click(button.abrirDropdown());
  $("#chart").click(button.abrirChart());
  $("#button-nave1").click(button.changeNave("1"));
  $("#button-nave2").click(button.changeNave("2"));
  $("#button-nave3").click(button.changeNave("3"));
  $("#comprar-button").click(button.comprarCreditos);
  $("#doar-button").click(button.doarCreditos);
  $("#usuario-button").click(button.changeUsername);
  $("#password-button").click(button.changePassword);
});

function formatDate(date) {
  if (date != null) {
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    return (
      day +
      "/" +
      monthIndex +
      "/" +
      year +
      " " +
      hours +
      ":" +
      minutes +
      ":" +
      seconds
    );
  }
}
