class Server {
  constructor(game, socket) {
    this._game = game;
    this._socket = socket;
  }

  handleDescer() {
    this._game.getPoll().descer();
    this._socket.emit("updateall", game.getAll());
  }
  handleSubir() {
    this._game.getPoll().subir();
    this._socket.emit("updateall", game.getAll());
  }
  handleReduzir() {
    this._game.getDeadlineTimer().reduce();
    this._socket.emit("updateall", game.getAll());
  }
}

const express = require("express");
const socket = require("socket.io");
const body_parser = require("body-parser");
const Game = require("./Game");
const Database = require("./Database");

var app = express();
var server = app.listen(3000);
var io = socket(server);

app.use(body_parser.urlencoded({ extended: false }));
app.use(body_parser.json());

//-------------------------------//

const database = new Database();
const game = new Game(database, io);
const serverObj = new Server(game, io);


io.on("connection", socket => {
  console.log("id connected:", socket.id);
  socket.emit("updateall", game.getAll());
  socket.on("subir", serverObj.handleSubir.bind(serverObj));
  socket.on("descer", serverObj.handleDescer.bind(serverObj));
  socket.on("reduzir", serverObj.handleReduzir.bind(serverObj));
});

database.connect(() => {
  game.start(true);
});
