"use strict";
const config = require("./configuracao.json").Saver;

module.exports = class Saver {
  constructor(database) {
    console.log("Criando o saver...");
    this._database = database;

    this._timers = [];
    this._polls = [];
    this._naves = [];
    this._charts = [];
    this._users = [];
  }

  _update(callback) {
    for (var i = 0; i < this._timers.length; i++)
      this._database.saveTimer(this._timers[i].getTimerJson());
    for (var i = 0; i < this._polls.length; i++)
      this._database.savePoll(this._polls[i].getPollJson());
    for (var i = 0; i < this._naves.length; i++)
      this._database.saveNave(this._naves[i].getNaveJson());
    for (var i = 0; i < this._charts.length; i++)
      this._database.saveChart(this._charts[i].getPontos());
    for (var i = 0; i < this._users.length; i++) {
      this._database.saveUser(this._users[i].getUserJson());
    }

    this.save();
  }

  save() {
    setTimeout(
      this._update.bind(this),
      config.SAVER_AMOSTRAGEM_EM_MILISSEGUNDOS
    );
  }

  addTimer(timer) {
    if (timer != null) this._timers.push(timer);
  }

  addPoll(poll) {
    if (poll != null) this._polls.push(poll);
  }

  addNave(nave) {
    if (nave != null) this._naves.push(nave);
  }

  addChart(chart) {
    if (chart != null) this._charts.push(chart);
  }

  addUser(user) {
    if (user != null) this._users.push(user);
  }

  setDatabase(database) {
    if (database != null) this._database = database;
  }
};
