"use strict";

module.exports = class Database {
  constructor() {
    console.log("Criando a database...");
    this._MongoClient = require("mongodb").MongoClient;
    this._db = null;
  }

  connect(callback) {
    console.log("Conectando ao mongodb...");
    var self = this;
    this._MongoClient.connect(
      "mongodb://localhost:27017",
      { useNewUrlParser: true },
      (error, db) => {
        if (error) console.log("ERRO AO CONECTAR AO MONGO DB");
        else {
          self._db = db;
          if (callback) callback();
        }
      }
    );
  }
  // ------------------------------NAVE-----------------------------//
  getNave(callback) {
    this._db
      .db("prizeship_v2")
      .collection("nave")
      .findOne({ nave: { $exists: true } }, (error, nave_json) => {
        if (error)
          console.log(
            "ERRO NA CHAMADA DE 'findOne()' EM 'getNave()' --> database.js"
          );
        else {
          if (nave_json) {
            if (callback) callback(nave_json.nave);
            else console.log("'getNave()' NEEDS A CALLBACK --> database.js");
          } else console.log("NAVE OBJECT NOT FOUND IN DB");
        }
      });
  }

  saveNave(nave_json) {
    if (nave_json)
      this._db
        .db("prizeship_v2")
        .collection("nave")
        .updateOne(
          { nave: { $exists: true } },
          { $set: { nave: nave_json } },
          { upsert: true }
        );
    else console.log("TRYING TO SAVE TO DB AN UNDEFINED NAVE");
  }
  // ------------------------------POOL-----------------------------//
  getPoll(callback) {
    this._db
      .db("prizeship_v2")
      .collection("poll")
      .findOne({ poll: { $exists: true } }, (error, poll_json) => {
        if (error)
          console.log(
            "ERRO NA CHAMADA DE 'findOne()' EM 'getPoll()' --> database.js"
          );
        else {
          if (poll_json) {
            console.log(poll_json.poll);
            if (callback) callback(poll_json.poll);
            else console.log("'getPoll()' NEEDS A CALLBACK --> database.js");
          } else console.log("POLL OBJECT NOT FOUND IN DB");
        }
      });
  }

  savePoll(poll_json) {
    if (poll_json)
      this._db
        .db("prizeship_v2")
        .collection("poll")
        .updateOne(
          { poll: { $exists: true } },
          { $set: { poll: poll_json } },
          { upsert: true }
        );
    else console.log("TRYING TO SAVE TO DB AN UNDEFINED POLL");
  }
  // ------------------------------TIMERS-----------------------------//
  getTimer(label, callback) {
    this._db
      .db("prizeship_v2")
      .collection("timers")
      .findOne({ label: label }, (error, timer_json) => {
        if (error)
          console.log(
            "ERRO NA CHAMADA DE 'findOne()' EM 'getTimer()' --> database.js"
          );
        else {
          if (timer_json) {
            if (callback)
              //o json do timer ja e o proprio objeto encontrado pela db
              callback(timer_json);
            else console.log("'getTimer()' NEEDS A CALLBACK --> database.js");
          } else console.log("TIMER OBJECT NOT FOUND IN DB");
        }
      });
  }

  saveTimer(timer_json) {
    if (timer_json)
      this._db
        .db("prizeship_v2")
        .collection("timers")
        .updateOne(
          { label: timer_json.label },
          { $set: { values: timer_json.values } },
          { upsert: true }
        );
    else console.log("TRYING TO SAVE TO DB AN UNDEFINED CDTIMER");
  }
  // ------------------------------CHART-----------------------------//
  getChart(callback) {
    this._db
      .db("prizeship_v2")
      .collection("chart")
      .findOne({ chart_array: { $exists: true } }, (error, chart) => {
        if (error)
          console.log(
            "ERRO NA CHAMADA DE 'findOne()' EM 'getTimer()' --> database.js"
          );
        else {
          if (chart) {
            if (callback) callback(chart.chart_array);
            else console.log("'getChart()' NEEDS A CALLBACK --> database.js");
          } else console.log("CHART OBJECT NOT FOUND IN DB");
        }
      });
  }

  saveChart(chart_array) {
    if (chart_array)
      this._db
        .db("prizeship_v2")
        .collection("chart")
        .updateOne(
          { chart_array: { $exists: true } },
          { $set: { chart_array: chart_array } },
          { upsert: true }
        );
    else console.log("TRYING TO SAVE TO DB AN UNDEFINED CHART ARRAY");
  }
  // ------------------------------USER-----------------------------//
  async getUsers(callback) {
    console.log("ta aqui no database");
    let user_array = await this._db
      .db("prizeship_v2")
      .collection("users")
      .find({ user: { $exists: true } })
      .toArray();

    callback(user_array);
  }

  saveUser(user_json) {
    if (user_json)
      this._db
        .db("prizeship_v2")
        .collection("users")
        .updateOne(
          { "user.id": user_json.id },
          { $set: { user: user_json } },
          { upsert: true }
        );
    else console.log("TRYING TO SAVE TO DB AN UNDEFINED USER");
  }
};
