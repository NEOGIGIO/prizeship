"use strict";

module.exports = class User {
  constructor() {
    console.log("Criando um usuario...");
    this._creditos = 0;
    this._id = User.incrementId();
  }

  static incrementId() {
    if (!this.latestId) this.latestId = 1;
    else this.latestId++;
    return this.latestId;
  }

  buyCredits(quant) {
    if (quant != null) this._creditos += quant;
  }

  getUserJson() {
    return {
      id: this._id,
      creditos: this._creditos
    };
  }

  getCreditos() {
    return this._creditos;
  }

  setUser(user_json) {
    if (user_json != null) this._creditos = user_json.creditos;
  }
};
